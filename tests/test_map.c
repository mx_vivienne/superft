#include <stddef.h>
#include <stdio.h>
#include "ft_map.h"
#include "CuTest.h"

static void test_mapinsert(CuTest *tc)
{
	t_map	*map = NULL;
	char	*pairs[] = {
		"hello", "world",		"foo", "bar",		"test", "",
		"", "test",				"key", "value",		"foob", "ar",
		NULL, NULL
	};
	
	for (int i = 0; pairs[i]; i += 2)
		CuAssert(tc, "mapinsert failed", ft_mapinsert(&map, pairs[i], pairs[i + 1]));
	
	for (int i = 0; pairs[i]; i += 2)
	{
		CuAssertStrEquals_Msg(tc, "returned value wrong",
			pairs[i + 1], ft_mapsearch(map, pairs[i]));
		CuAssert(tc, "mapinsert accepted duplicate key",
			!ft_mapinsert(&map, pairs[i], pairs[i + 1]));
	}

	ft_mapclear(&map);
}

static void test_mapupdate(CuTest *tc)
{
	t_map	*map = NULL;
	char	*pairs[] = {
		"hello", "world",		"hello", "hello",	"foo", "bar",
		"test", "",				"", "test",			"key", "value",
		"Foo", "bar",			"foo", "BAR",		"", "",
		"hello", "there",		NULL, NULL
	};
	char	*result[] = {
		"hello", "there",		"foo", "BAR",		"test", "",
		"", "",					"key", "value",		"Foo", "bar",
		NULL, NULL
	};
	
	for (int i = 0; pairs[i]; i += 2)
		CuAssert(tc, "mapupdate failed", ft_mapupdate(&map, pairs[i], pairs[i + 1]));
	
	for (int i = 0; result[i]; i += 2)
	{
		CuAssertStrEquals_Msg(tc, "returned value wrong",
			result[i + 1], ft_mapsearch(map, result[i]));
	}

	ft_mapclear(&map);
}

CuSuite *map_getsuite(char **name)
{
	CuSuite	*suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, test_mapinsert);
	SUITE_ADD_TEST(suite, test_mapupdate);

	*name = "map";
	return (suite);
}