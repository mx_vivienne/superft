#include <stdio.h>
#include "CuTest.h"
#include "tests.h"

static CuSuite *(*suites[])(char**) = {
	str_getsuite,
	pair_getsuite,
	map_getsuite,
	NULL
};

void run_all(void)
{
	CuString	*output;
	CuSuite		*suite;
	char		*suite_name;

	for (int i = 0; suites[i]; i++)
	{
		output = CuStringNew();
		suite = suites[i](&suite_name);

		printf("======= %s =======\n\n", suite_name);
		CuSuiteRun(suite);
		CuSuiteSummary(suite, output);
		CuSuiteDetails(suite, output);
		printf("%s\n\n\n", output->buffer);

		CuStringDelete(output);
		CuSuiteDelete(suite);
	}
}

int main(void)
{
	run_all();
	return (0);
}