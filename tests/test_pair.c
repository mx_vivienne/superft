#include <stdlib.h>
#include <string.h>
#include "ft_map.h"
#include "CuTest.h"

int	pair_has_key(void *key, void *pair);

static void test_pairnew(CuTest *tc)
{
	t_pair	*pair = NULL;
	char	*pairs[] = {
		"hello", "world",		"foo", "bar",		"test", "",
		"", "test",				"key", "value",		"Foo", "bar",
		NULL, NULL
	};
	
	for (int i = 0; pairs[i]; i += 2)
	{
		pair = ft_pairnew(pairs[i], pairs[i + 1]);
		CuAssert(tc, "pairnew failed", !!pair);
		CuAssertStrEquals_Msg(tc, "key wrong", pairs[i], pair->key);
		CuAssertStrEquals_Msg(tc, "value wrong", pairs[i + 1], pair->value);
		free(pair->key);
		free(pair->value);
		free(pair);
	}
}

static void test_pairset(CuTest *tc)
{
	t_pair	*pair = NULL;
	char	*pairs[] = {
		"hello", "world", "World",		"foo", "bar", "foo",		"test", "", "",
		"", "test", "testtest",			"key", "value",	"value",	"Foo", "bar", "Foo",
		NULL, NULL, NULL
	};
	
	for (int i = 0; pairs[i]; i += 3)
	{
		pair = ft_pairnew(pairs[i], pairs[i + 1]);
		CuAssert(tc, "pairnew failed", !!pair);
		CuAssert(tc, "pairset failed", ft_pairset(&pair, pairs[i + 2]));
		CuAssertStrEquals_Msg(tc, "key wrong", pairs[i], pair->key);
		CuAssertStrEquals_Msg(tc, "value wrong", pairs[i + 2], pair->value);
		free(pair->key);
		free(pair->value);
		free(pair);
	}
}

static void test_pair_has_key(CuTest *tc)
{
	t_pair	*pair = NULL;
	char	*pairs[] = {
		"hello", "world", "World",		"foo", "bar", "foo",		"test", "", "",
		"", "test", "testtest",			"key", "value",	"value",	"Foo", "bar", "Foo",
		NULL, NULL, NULL
	};

	for (int i = 0; pairs[i]; i += 3)
	{
		pair = ft_pairnew(pairs[i], pairs[i + 1]);
		CuAssert(tc, "pairnew failed", !!pair);
		if (strcmp(pairs[i], pairs[i + 2]) == 0)
		{
			CuAssert(tc, "pair_has_key returned false, "
				"but the pair contains this key", pair_has_key(pairs[2], pair));
		}
		else
		{
			CuAssert(tc, "pair_has_key returned true, "
				"but the pair doesnt contain this key", pair_has_key(pairs[2], pair));
		}
		free(pair->key);
		free(pair->value);
		free(pair);
	}
}

CuSuite *pair_getsuite(char **name)
{
	CuSuite	*suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, test_pairnew);
	SUITE_ADD_TEST(suite, test_pairset);
	SUITE_ADD_TEST(suite, test_pair_has_key);

	*name = "pair";
	return (suite);
}