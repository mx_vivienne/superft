#include <stdio.h>
#include <stdlib.h>
#include "ft_string.h"
#include "ft_str.h"
#include "CuTest.h"

static void	test_init(CuTest *tc)
{
	t_string string;
	int input_amount = 5;
	size_t inputs[] = {0, 1, 2, 15, 1000};

	for (int i = 0; i < input_amount; i++)
	{
		string = (t_string) {NULL, 0};

		CuAssert(tc, "returned wrong value",
			(!!inputs[i]) == ft_string_init(&string, inputs[i]));

		if (inputs[i] == 0)
		{
			CuAssert(tc, "capacity should be 0", string.capacity == 0);
			continue ;
		}

		CuAssert(tc, "capacity too small", string.capacity >= inputs[i]);
		CuAssert(tc, "buffer was not set", string.content != NULL);
		CuAssertStrEquals_Msg(tc, "content is wrong", "", string.content);

		free(string.content);
	}
}

static void	test_from(CuTest *tc)
{
	t_string string;
	int input_amount = 5;
	char *inputs[] = {
		"", "a", "hi", "hello world", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
	};

	for (int i = 0; i < input_amount; i++)
	{
		string = (t_string) {NULL, 0};

		CuAssert(tc, "returned false", ft_string_from(&string, inputs[i]));
		CuAssert(tc, "capacity too small", string.capacity >= ft_strlen(inputs[i]) + 1);
		CuAssert(tc, "buffer was not set", string.content != NULL);
		CuAssertStrEquals_Msg(tc, "content is wrong", inputs[i], string.content);
		
		free(string.content);
	}
}

static void test_dup(CuTest *tc)
{
	t_string str1;
	t_string str2;
	int input_amount = 5;
	char *inputs[] = {
		"", "a", "hi", "hello world", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
	};

	for (int i = 0; i < input_amount; i++)
	{
		CuAssert(tc, "string_from failed", ft_string_from(&str1, inputs[i]));

		str2 = (t_string) {NULL, 0};
		CuAssert(tc, "returned false", ft_string_dup(&str1, &str2));
		CuAssert(tc, "capacity wrong", str1.capacity == str2.capacity);
		CuAssertStrEquals_Msg(tc, "content wrong", str1.content, str2.content);

		free(str1.content);
		free(str2.content);
	}
}

static void test_capacity(CuTest *tc)
{
	t_string string;
	int input_amount = 8;
	size_t inputs[] = {
		1, 1,		1, 2,		1, 10,		1, 1000,
		50, 10,		50, 50,		50, 51,		50, 100
	};

	for (int i = 0; i < input_amount; i++)
	{
		CuAssert(tc, "init failed", ft_string_init(&string, inputs[i * 2]));
		CuAssert(tc, "resizing failed", ft_string_capacity(&string, inputs[i * 2 + 1]));
		CuAssert(tc, "new capacity too small",
			string.capacity >= inputs[i * 2] && string.capacity >= inputs[i * 2 + 1]);

		free(string.content);
	}
}

CuSuite	*str_getsuite(char **name)
{
	CuSuite	*suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, test_init);
	SUITE_ADD_TEST(suite, test_from);
	SUITE_ADD_TEST(suite, test_dup);
	SUITE_ADD_TEST(suite, test_capacity);
	
	*name = "str";
	return (suite);
}