#ifndef TESTS_H
# define TESTS_H

#include "CuTest.h"

CuSuite *str_getsuite(char **name);
CuSuite *pair_getsuite(char **name);
CuSuite *map_getsuite(char **name);

#endif
