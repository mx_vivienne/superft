/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_pairset.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 21:27:46 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 21:27:48 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"
#include "ft_map.h"

/*
** Change the value stored in a pair, return false if any allocations failed.
** `new_value` always needs to be freed by the caller.
*/
bool	ft_pairset(t_pair **pair, char *new_value)
{
	char	*value_dup;

	value_dup = ft_strdup(new_value);
	if (!value_dup)
		return (false);
	free((*pair)->value);
	(*pair)->value = value_dup;
	return (true);
}
