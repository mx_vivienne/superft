/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_mapupdate.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 20:47:07 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 20:47:09 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdbool.h>
#include "ft_list.h"
#include "ft_map.h"

/*
** Insert the key-value pair if the key doesnt exist yet, or modify the value
** to the corresponding key otherwise.
** `key` and `value` always need to be freed by the caller.
*/
bool	ft_mapupdate(t_map **map, char *key, char *value)
{
	t_pair	*pair;
	t_list	*elem;

	pair = ft_mapsearch_pair(*map, key);
	if (pair)
		return (ft_pairset(&pair, value));
	pair = ft_pairnew(key, value);
	if (!pair)
		return (false);
	elem = ft_lstnew(pair);
	if (!elem)
	{
		ft_pairdestroy(&pair);
		return (false);
	}
	ft_lstadd_front((t_list **)map, elem);
	return (true);
}
