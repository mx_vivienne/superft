/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_mapprint.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/20 01:35:01 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/20 01:35:02 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_map.h"
#include "ft_list.h"

int	ft_mapprint(t_map *map, char *format, char *delim)
{
	t_list	*elem;
	int		ret;
	int		printed;

	printed = 0;
	elem = (t_list *)map;
	while (elem)
	{
		ret = ft_pairprint((t_pair *)elem->content, format);
		if (ret < 0)
			return (ret);
		printed += ret;
		if (elem->next)
		{
			ret = printf("%s", delim);
			if (ret < 0)
				return (ret);
			printed += ret;
		}
		elem = elem->next;
	}
	return (printed);
}
