/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_mapsearch.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 20:47:18 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 20:47:20 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map.h"
#include "ft_string.h"

/*
** Search for an element by its key, and return its value, or NULL if the map
** doesnt contain this key.
*/
char	*ft_mapsearch(t_map *map, char *key)
{
	t_list	*elem;
	t_pair	*pair;

	if (!map)
		return (NULL);
	elem = (t_list *)map;
	while (elem)
	{
		pair = (t_pair *)elem->content;
		if (ft_strncmp(key, pair->key, ft_strlen(key) + 1) == 0)
			return (pair->value);
		elem = elem->next;
	}
	return (NULL);
}
