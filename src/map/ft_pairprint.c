/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_pairprint.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/20 01:51:00 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/20 01:51:01 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_map.h"

int	ft_pairprint(t_pair *pair, char *format)
{
	return (printf(format, pair->key, pair->value));
}
