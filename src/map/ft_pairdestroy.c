/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_pairdestroy.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 20:27:23 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 20:27:24 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_map.h"

/*
** Free the pair and any strings it contains.
*/
void	ft_pairdestroy(t_pair **pair)
{
	free((*pair)->key);
	free((*pair)->value);
	free(*pair);
	*pair = NULL;
}
