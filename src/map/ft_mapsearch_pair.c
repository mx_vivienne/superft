/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_mapsearch_pair.c                                :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 23:59:43 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 23:59:44 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map.h"
#include "ft_string.h"

/*
** Search for an element by its key, and return its value, or NULL if the map
** doesnt contain this key.
*/
t_pair	*ft_mapsearch_pair(t_map *map, char *key)
{
	t_list	*elem;
	t_pair	*pair;

	if (!map)
		return (NULL);
	elem = (t_list *)map;
	while (elem)
	{
		pair = (t_pair *)elem->content;
		if (ft_strncmp(key, pair->key, ft_strlen(key) + 1) == 0)
			return (pair);
		elem = elem->next;
	}
	return (NULL);
}
