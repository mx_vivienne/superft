/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_mapinsert.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 20:46:59 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 20:47:00 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include "ft_list.h"
#include "ft_map.h"

/*
** Insert a key-value pair into the map. Return false if any allocations fail,
** or if the map already contains a pair with this key.
** `key` and `value` always have to be freed by the caller.
*/
bool	ft_mapinsert(t_map **map, char *key, char *value)
{
	t_pair	*pair;
	t_list	*elem;

	if (ft_mapsearch(*map, key))
		return (false);
	pair = ft_pairnew(key, value);
	if (!pair)
		return (false);
	elem = ft_lstnew(pair);
	if (!elem)
	{
		ft_pairdestroy(&pair);
		return (false);
	}
	ft_lstadd_front((t_list **)map, elem);
	return (true);
}
