/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_pairnew.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 20:27:16 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 20:27:17 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_map.h"
#include "ft_string.h"

/*
** Create a new pair of key-value strings, return NULL if allocation failed.
** `key` and `value` always have to be freed by the caller.
*/
t_pair	*ft_pairnew(char *key, char *value)
{
	t_pair	*pair;
	char	*key_dup;
	char	*value_dup;

	pair = malloc(sizeof(t_pair));
	if (!pair)
		return (NULL);
	key_dup = ft_strdup(key);
	if (!key_dup)
	{
		free(pair);
		return (NULL);
	}
	value_dup = ft_strdup(value);
	if (!value_dup)
	{
		free(pair);
		free(key_dup);
		return (NULL);
	}
	*pair = (t_pair){key_dup, value_dup};
	return (pair);
}
