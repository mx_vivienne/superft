/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_mapclear.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 20:47:39 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 20:47:40 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map.h"
#include "ft_list.h"

void	del_pair(void *pair);

/*
** Free the entire map and all pairs and strings contained in it.
*/
void	ft_mapclear(t_map **map)
{
	ft_lstclear((t_list **)map, *del_pair);
}
