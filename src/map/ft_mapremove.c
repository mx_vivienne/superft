/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_mapremove.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 20:47:28 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 20:47:30 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map.h"
#include "ft_string.h"

void	del_pair(void *pair);

static int	pair_has_key(void *key, void *pair)
{
	return (ft_strncmp((char *)key,
			((t_pair *)pair)->key,
			ft_strlen((char *)key)));
}

/*
** Remove the element with the given key.
*/
void	ft_mapremove(t_map **map, char *key)
{
	ft_lstremove((t_list **)map, key, *pair_has_key, *del_pair);
}
