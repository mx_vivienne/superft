/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   del_pair.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/08 00:07:36 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/08 00:07:39 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_map.h"

void	del_pair(void *pair)
{
	t_pair	*p;

	p = (t_pair *)pair;
	ft_pairdestroy(&p);
}
