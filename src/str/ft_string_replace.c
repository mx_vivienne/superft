/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_string_replace.c                                :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/12 02:21:07 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/12 02:21:09 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include <stddef.h>
#include "ft_str.h"
#include "ft_string.h"

bool	ft_string_replace(t_string *str, size_t start, size_t end,
			char *replacement)
{
	size_t	rep_len;
	size_t	str_len;
	size_t	range;

	str_len = ft_strlen(str->content);
	if (end > str_len || end < start)
		return (false);
	rep_len = ft_strlen(replacement);
	range = end - start;
	ft_string_capacity(str, str_len + 1 + rep_len - range);
	ft_memmove(str->content + end + rep_len - range,
		str->content + end, str_len + 1 - end);
	ft_memcpy(str->content + start, replacement, rep_len);
	return (true);
}
