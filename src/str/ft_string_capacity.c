/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_string_capacity.c                               :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/12 01:49:41 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/12 01:49:42 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdbool.h>
#include "ft_string.h"
#include "ft_str.h"

bool	ft_string_capacity(t_string *str, size_t capacity)
{
	char	*new_str;

	if (capacity <= str->capacity)
		return (true);
	new_str = malloc(capacity);
	if (!new_str)
		return (false);
	ft_strlcpy(new_str, str->content, capacity);
	free(str->content);
	str->content = new_str;
	str->capacity = capacity;
	return (true);
}
