/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_string_from.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/12 04:58:37 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/12 04:58:39 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include <stdlib.h>
#include "ft_str.h"
#include "ft_string.h"

bool	ft_string_from(t_string *str, char *content)
{
	char	*dup;
	size_t	size;

	size = ft_strlen(content) + 1;
	dup = malloc(size);
	if (!dup)
		return (false);
	ft_strlcpy(dup, content, size);
	*str = (t_string){dup, ft_strlen(dup) + 1};
	return (true);
}
