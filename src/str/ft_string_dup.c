/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_string_dup.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/12 05:17:44 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/12 05:17:45 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_str.h"
#include "ft_string.h"

bool	ft_string_dup(t_string *original, t_string *copy)
{
	char	*dup;

	if (original->capacity == 0)
	{
		*copy = (t_string){NULL, 0};
		return (true);
	}
	dup = malloc(original->capacity);
	if (!dup)
		return (false);
	ft_strlcpy(dup, original->content, original->capacity);
	*copy = (t_string){dup, original->capacity};
	return (true);
}
