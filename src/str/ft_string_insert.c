/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_string_insert.c                                 :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/12 03:00:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/12 03:00:55 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

bool	ft_string_insert(t_string *str, size_t index, char *src)
{
	return (ft_string_replace(str, index, index, src));
}
