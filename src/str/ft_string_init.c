/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_string_init.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/12 01:49:29 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/12 01:49:30 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdbool.h>
#include "ft_str.h"

bool	ft_string_init(t_string *str, size_t capacity)
{
	char	*mem;

	if (!capacity)
		return (false);
	mem = malloc(capacity);
	if (!mem)
		return (false);
	*mem = '\0';
	*str = (t_string){mem, capacity};
	return (true);
}
