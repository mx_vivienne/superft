/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_string_free.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/12 01:49:54 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/12 01:49:55 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_str.h"

void	ft_string_free(t_string *str)
{
	free(str->content);
	str->capacity = 0;
}
