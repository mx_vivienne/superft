/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_string_remove.c                                 :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/12 03:07:01 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/12 03:07:02 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdbool.h>
#include "ft_str.h"
#include "ft_string.h"

bool	ft_string_remove(t_string *str, size_t start, size_t end)
{
	size_t	len;

	len = ft_strlen(str->content);
	if (end > len || start > end)
		return (false);
	ft_memmove(str->content + start, str->content + end,
		ft_strlen(str->content) + 1 - end);
	return (true);
}
