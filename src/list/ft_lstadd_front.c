/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstadd_front.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:34:31 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:34:32 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_list.h"

void	ft_lstadd_front(t_list **lst, t_list *elem)
{
	if (lst == NULL || elem == NULL)
		return ;
	elem->next = *lst;
	*lst = elem;
}
