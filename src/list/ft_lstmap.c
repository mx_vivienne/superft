/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstmap.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:35:32 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:35:33 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_list.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*new;
	t_list	*elem;
	t_list	*next;

	if (lst == NULL || f == NULL || *f == NULL || del == NULL)
		return (NULL);
	elem = ft_lstnew((f)(lst->content));
	if (elem == NULL)
		return (NULL);
	new = elem;
	while (lst->next != NULL)
	{
		lst = lst->next;
		next = ft_lstnew((f)(lst->content));
		if (next == NULL)
		{
			ft_lstclear(&new, del);
			return (NULL);
		}
		elem->next = next;
		elem = next;
	}
	return (new);
}
