/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstadd_back.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:35:26 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:35:37 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_list.h"

void	ft_lstadd_back(t_list **lst, t_list *elem)
{
	t_list	*end;

	if (lst == NULL || elem == NULL)
		return ;
	if (*lst == NULL)
		*lst = elem;
	else
	{
		end = ft_lstlast(*lst);
		if (end == NULL)
			return ;
		end->next = elem;
	}
}
