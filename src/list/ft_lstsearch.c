/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstsearch.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 22:44:33 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 22:44:35 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_list.h"

/*
** Return the first element whose content is equal to `item`, or NULL if none
** are found. Equality is tested with `cmp`, which should return 0 when given
** two arguments considered equal.
*/
t_list	*ft_lstsearch(t_list **lst, void *item, int (*cmp)(void*, void*))
{
	t_list	*elem;

	elem = *lst;
	while (elem)
	{
		if ((*cmp)(item, elem->content) == 0)
			return (elem);
		elem = elem->next;
	}
	return (NULL);
}
