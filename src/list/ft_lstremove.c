/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstremove.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 22:44:43 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/16 00:52:58 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_list.h"

/*
** Remove all elements whose content is equal to `item`. Equality is tested
** with `cmp`, which should return 0 for arguments considered equal.
** `del` is used to free removed elements.
*/
void	ft_lstremove(t_list **lst, void *item, int (*cmp)(void*, void*),
	void (*del)(void*))
{
	t_list	*curr;
	t_list	*prev;

	curr = *lst;
	prev = NULL;
	while (curr)
	{
		if ((*cmp)(item, curr->content) != 0)
		{
			prev = curr;
			curr = curr->next;
			continue ;
		}
		curr = curr->next;
		if (!prev)
		{
			ft_lstdelone(*lst, del);
			*lst = curr;
			continue ;
		}
		ft_lstdelone(prev->next, del);
		prev->next = curr;
	}
}
