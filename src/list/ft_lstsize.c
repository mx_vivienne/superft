/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstsize.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:34:29 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:34:33 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_list.h"

int	ft_lstsize(t_list *lst)
{
	t_list	*elem;
	int		len;

	elem = lst;
	len = 0;
	while (elem != NULL)
	{
		elem = elem->next;
		len++;
	}
	return (len);
}
