/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstclear.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:35:30 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:35:35 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_list.h"

void	ft_lstclear(t_list **lst, void (*del)(void*))
{
	t_list	*next;

	if (lst == NULL)
		return ;
	if ((*lst)->next != NULL)
	{
		next = (*lst)->next;
		ft_lstclear(&next, del);
	}
	ft_lstdelone(*lst, del);
	*lst = NULL;
}
