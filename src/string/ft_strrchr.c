/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strrchr.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/23 23:40:28 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/23 23:40:33 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_strrchr(const char *s, int c)
{
	size_t	offset;
	char	*chr;

	offset = 0;
	chr = NULL;
	while (1)
	{
		if (s[offset] == c)
			chr = (char *)s + offset;
		if (s[offset] == '\0')
			return (chr);
		offset++;
	}
}
