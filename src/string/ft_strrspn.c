/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strrspn.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/25 00:35:02 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/25 00:35:04 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_string.h"

size_t	ft_strrspn(const char *s, const char *accept)
{
	size_t	offset;
	size_t	start;

	offset = 0;
	start = 0;
	while (s[offset])
	{
		if (!ft_isinset(accept, s[offset]))
			start = offset + 1;
		offset++;
	}
	return (offset - start);
}
