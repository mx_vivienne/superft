/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strlcpy.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/23 23:48:36 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/23 23:48:41 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

size_t	ft_strlcpy(char *dst, const char *src, size_t dstsize)
{
	size_t	offset;

	offset = 0;
	while (offset + 1 < dstsize)
	{
		if (src[offset] == '\0')
			break ;
		dst[offset] = src[offset];
		offset++;
	}
	if (dstsize > 0)
		dst[offset] = '\0';
	while (src[offset] != '\0')
		offset++;
	return (offset);
}
