/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strndup.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 04:13:56 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 04:13:58 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

char	*ft_strndup(const char *s, size_t n)
{
	char	*dup;
	size_t	offset;
	size_t	len;

	len = ft_strnlen(s, n);
	dup = malloc((len + 1) * sizeof(char));
	if (!dup)
		return (NULL);
	offset = 0;
	while (offset < len)
	{
		dup[offset] = s[offset];
		offset++;
	}
	dup[offset] = '\0';
	return (dup);
}
