/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memchr.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:53:57 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:54:06 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	byte;
	unsigned char	*mem;
	size_t			offset;

	byte = (unsigned char)c;
	mem = (unsigned char *)s;
	offset = 0;
	while (offset < n)
	{
		if (byte == mem[offset])
			return (mem + offset);
		offset++;
	}
	return (NULL);
}
