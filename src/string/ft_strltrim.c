/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strltrim.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/14 19:41:15 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/14 19:41:16 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

char	*ft_strltrim(char const *s1, char const *set)
{
	size_t	offset;
	size_t	size;

	if (s1 == NULL || set == NULL)
		return (NULL);
	offset = 0;
	size = ft_strlen(s1);
	while (offset < size && ft_isinset(set, s1[offset]))
		offset++;
	return (ft_strndup((char *)s1 + offset, size - offset));
}
