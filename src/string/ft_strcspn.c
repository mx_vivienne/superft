/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strcspn.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 03:55:49 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 03:55:50 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_string.h"

size_t	ft_strspn(const char *s, const char *accept)
{
	size_t	offset;

	offset = 0;
	while (s[offset])
	{
		if (!ft_isinset(accept, s[offset]))
			return (offset);
		offset++;
	}
	return (offset);
}
