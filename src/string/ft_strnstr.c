/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strnstr.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/23 23:40:30 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/23 23:40:32 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	boffset;
	size_t	loffset;
	size_t	substr;

	boffset = 0;
	loffset = 0;
	substr = 0;
	while (little[loffset] != '\0')
	{
		if (big[boffset] == '\0' || boffset >= len)
			return (NULL);
		if (big[boffset] == little[loffset])
		{
			boffset++;
			loffset++;
		}
		else
		{
			substr++;
			boffset = substr;
			loffset = 0;
		}
	}
	return ((char *)big + substr);
}
