/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memcmp.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:53:57 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:54:03 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t			offset;
	unsigned char	*m1;
	unsigned char	*m2;

	offset = 0;
	m1 = (unsigned char *)s1;
	m2 = (unsigned char *)s2;
	while (offset < n)
	{
		if (m1[offset] != m2[offset])
			return (m1[offset] - m2[offset]);
		offset++;
	}
	return (0);
}
