/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strreplace.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/14 01:14:56 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/14 01:58:35 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include <stddef.h>
#include "ft_string.h"
#include "ft_stdlib.h"

bool	ft_strreplace(char **str, size_t start, size_t end, char *replacement)
{
	size_t	rep_len;
	size_t	str_len;
	size_t	range;

	str_len = ft_strlen(*str);
	if (end > str_len || end < start)
		return (false);
	rep_len = ft_strlen(replacement);
	range = end - start;
	*str = ft_resize(*str, str_len, str_len + 1 + rep_len - range);
	if (!(*str))
		return (false);
	ft_memmove(*str + end + rep_len - range, *str + end, str_len + 1 - end);
	ft_memcpy(*str, replacement, rep_len);
	return (true);
}
