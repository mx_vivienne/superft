/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strtrim.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/23 23:56:29 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/23 23:56:30 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	size_t	start;
	size_t	end;
	size_t	size;

	if (s1 == NULL || set == NULL)
		return (NULL);
	start = 0;
	size = ft_strlen(s1);
	while (start < size && ft_isinset(set, s1[start]))
		start++;
	end = size;
	while (end > start)
	{
		if (!ft_isinset(set, s1[end - 1]))
			break ;
		end--;
	}
	return (ft_strndup((char *)s1 + start, end - start));
}
