/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_bzero.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:47:29 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:47:30 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	ft_bzero(void *s, size_t n)
{
	char		*ptr;
	size_t		offset;

	offset = 0;
	ptr = (char *)s;
	while (offset < n)
	{
		ptr[offset] = 0;
		offset++;
	}
}
