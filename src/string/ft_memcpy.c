/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memcpy.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:54:00 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:54:03 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t			offset;
	unsigned char	*destmem;
	unsigned char	*srcmem;

	if (dest == NULL && src == NULL)
		return (NULL);
	offset = 0;
	destmem = (unsigned char *)dest;
	srcmem = (unsigned char *)src;
	while (offset < n)
	{
		destmem[offset] = srcmem[offset];
		offset++;
	}
	return (dest);
}
