/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strrcspn.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/25 00:35:03 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/25 00:35:04 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_string.h"

size_t	ft_strrcspn(const char *s, const char *reject)
{
	size_t	offset;
	size_t	start;

	offset = 0;
	start = 0;
	while (s[offset])
	{
		if (ft_isinset(reject, s[offset]))
			start = offset + 1;
		offset++;
	}
	return (offset - start);
}
