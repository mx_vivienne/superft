/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strchr.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/23 23:40:27 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/23 23:40:34 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_strchr(const char *s, int c)
{
	size_t	offset;

	offset = 0;
	while (s[offset] != c && s[offset] != '\0')
		offset++;
	if (s[offset] == c)
		return ((char *)s + offset);
	return (NULL);
}
