/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strchrnul.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/14 19:10:19 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/14 19:10:20 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_strchrnul(const char *s, int c)
{
	size_t	offset;

	offset = 0;
	while (s[offset] != c && s[offset] != '\0')
		offset++;
	return ((char *)s + offset);
}
