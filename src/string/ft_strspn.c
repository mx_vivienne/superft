/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strspn.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 03:55:39 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 03:55:41 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_string.h"

size_t	ft_strcspn(const char *s, const char *reject)
{
	size_t	offset;

	offset = 0;
	while (s[offset])
	{
		if (ft_isinset(reject, s[offset]))
			return (offset);
		offset++;
	}
	return (offset);
}
