/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strlcat.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/23 23:48:36 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/23 23:48:40 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_string.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t	offset;

	offset = 0;
	while (dst[offset] != '\0' && offset < dstsize)
		offset++;
	if (offset < dstsize)
		return (offset + ft_strlcpy(dst + offset, src, dstsize - offset));
	return (offset + ft_strlcpy(dst + offset, src, 0));
}
