/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strfield.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/14 19:17:13 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/14 19:17:14 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_strfield(char *line, int n, char delim, char **end)
{
	int		index;
	char	*start;

	index = 0;
	start = line;
	while (index < n)
	{
		start = ft_strchr(start, delim);
		if (!start)
			return (NULL);
		start++;
		index++;
	}
	*end = ft_strchrnul(start, delim);
	return (start);
}
