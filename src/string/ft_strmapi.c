/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strmapi.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/23 23:56:28 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/23 23:56:31 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	offset;
	char			*str;

	if (s == NULL || f == NULL)
		return (NULL);
	offset = 0;
	str = malloc(ft_strlen(s) + 1);
	if (str != NULL)
	{
		while (s[offset] != '\0')
		{
			str[offset] = (*f)(offset, s[offset]);
			offset++;
		}
		str[offset] = '\0';
	}
	return (str);
}
