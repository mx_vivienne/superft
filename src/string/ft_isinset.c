/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_isinset.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/23 23:56:28 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/23 23:56:31 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

int	ft_isinset(const char *set, char c)
{
	size_t	offset;

	offset = 0;
	while (set[offset] != '\0')
	{
		if (set[offset] == c)
			return (1);
		offset++;
	}
	return (0);
}
