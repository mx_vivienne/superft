/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memccpy.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:53:58 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:54:02 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t			offset;
	unsigned char	*destmem;
	unsigned char	*srcmem;

	offset = 0;
	destmem = (unsigned char *)dest;
	srcmem = (unsigned char *)src;
	while (offset < n)
	{
		destmem[offset] = srcmem[offset];
		if (destmem[offset] == (unsigned char)c)
			return ((void *)(destmem + offset + 1));
		offset++;
	}
	return (NULL);
}
