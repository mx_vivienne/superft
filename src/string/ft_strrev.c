/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strrev.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:31:38 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:31:39 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_string.h"

void	ft_strrev(char *str)
{
	size_t	offset;
	size_t	len;
	char	temp;

	len = ft_strlen(str);
	if (len > 1)
	{
		offset = 0;
		while (offset < len / 2)
		{
			temp = str[offset];
			str[offset] = str[len - offset - 1];
			str[len - offset - 1] = temp;
			offset++;
		}
	}
}
