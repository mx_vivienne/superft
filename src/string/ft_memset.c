/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memset.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:53:14 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:53:16 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memset(void *s, int c, size_t n)
{
	char	*ptr;
	size_t	offset;

	offset = 0;
	ptr = (char *)s;
	while (offset < n)
	{
		ptr[offset] = (char)c;
		offset++;
	}
	return (s);
}
