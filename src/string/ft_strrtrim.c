/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strrtrim.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/14 19:27:26 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/14 19:27:27 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

char	*ft_strrtrim(char const *s1, char const *set)
{
	size_t	offset;

	if (s1 == NULL || set == NULL)
		return (NULL);
	offset = ft_strlen(s1);
	while (offset > 0)
	{
		if (!ft_isinset(set, s1[offset - 1]))
			break ;
		offset--;
	}
	return (ft_strndup(s1, offset));
}
