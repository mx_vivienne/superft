/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memmove.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:53:59 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:54:02 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_string.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	size_t			offset;
	unsigned char	*destmem;
	unsigned char	*srcmem;

	if (dest == src || n == 0)
		return (dest);
	if (dest < src)
		return (ft_memcpy(dest, src, n));
	offset = n - 1;
	destmem = (unsigned char *)dest;
	srcmem = (unsigned char *)src;
	while (1)
	{
		destmem[offset] = srcmem[offset];
		if (0 == offset)
			return (dest);
		offset--;
	}
}
