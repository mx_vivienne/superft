/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strdup.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/23 23:48:35 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/23 23:48:41 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

char	*ft_strdup(const char *s)
{
	char	*dup;
	size_t	offset;

	dup = malloc(ft_strlen(s) + 1);
	if (NULL == dup)
		return (NULL);
	offset = 0;
	while (s[offset] != '\0')
	{
		dup[offset] = s[offset];
		offset++;
	}
	dup[offset] = '\0';
	return (dup);
}
