/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_split.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/31 00:15:50 by vwaterme      #+#    #+#                 */
/*   Updated: 2020/11/18 15:44:58 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

static char	*find_word(const char *s, char delim, char **startptr)
{
	size_t	offset;

	offset = 0;
	while (s[offset] == delim && s[offset] != '\0')
		offset++;
	if (s[offset] == '\0')
		return (NULL);
	if (startptr != NULL)
		*startptr = (char *)s + offset;
	while (s[offset] != delim && s[offset] != '\0')
		offset++;
	return ((char *)s + offset);
}

static void	*free_words(char **words, size_t count)
{
	size_t	pos;

	pos = 0;
	while (pos < count)
	{
		free(words[pos]);
		pos++;
	}
	free(words);
	return (NULL);
}

static size_t	count_words(const char *s, char c)
{
	size_t	amount;
	char	*end;

	end = (char *)s;
	amount = 0;
	while (1)
	{
		end = find_word(end, c, NULL);
		if (end == NULL)
			return (amount);
		amount++;
	}
}

char	**ft_split(char const *s, char c)
{
	char	**words;
	char	*start;
	char	*end;
	size_t	count;
	size_t	amount;

	if (s == NULL)
		return (NULL);
	amount = count_words(s, c);
	words = (char **)malloc((amount + 1) * sizeof(char *));
	if (words == NULL)
		return (NULL);
	count = 0;
	end = (char *)s;
	words[amount] = NULL;
	while (count < amount)
	{
		end = find_word(end, c, &start);
		words[count] = (char *)malloc((end - start + 1) * sizeof(char));
		if (words[count] == NULL)
			return (free_words(words, count));
		ft_strlcpy(words[count], start, end - start + 1);
		count++;
	}
	return (words);
}
