/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_resize.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 05:54:26 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 05:54:27 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

void	*ft_resize(void *ptr, size_t from, size_t to)
{
	char	*dest;
	size_t	min;

	if (!ptr)
		return (malloc(to));
	if (to == 0)
	{
		free(ptr);
		return (NULL);
	}
	dest = malloc(to);
	if (!dest)
		return (NULL);
	min = from;
	if (to < from)
		min = to;
	ft_memcpy(dest, ptr, min);
	free(ptr);
	return (dest);
}
