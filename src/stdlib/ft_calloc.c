/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_calloc.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:38:39 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:38:39 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_string.h"

void	*ft_calloc(size_t nmemb, size_t size)
{
	size_t	range;
	char	*mem;

	range = nmemb * size;
	if (nmemb != 0 && size != 0 && range / nmemb != size)
		return (NULL);
	mem = malloc(range);
	if (mem != NULL)
		ft_bzero((void *)mem, range);
	return ((void *)mem);
}
