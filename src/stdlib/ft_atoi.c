/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_atoi.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:33:56 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:34:01 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_ctype.h"

int	ft_atoi(const char *n)
{
	size_t			i;
	int				sign;
	unsigned int	val;

	i = 0;
	val = 0;
	sign = 1;
	while (n[i] == ' ' || n[i] == '\n' || n[i] == '\t'
		|| n[i] == '\f' || n[i] == '\r' || n[i] == '\v')
		i++;
	if (n[i] == '+' || n[i] == '-')
	{
		if (n[i] == '-')
			sign = -1;
		i++;
	}
	while (ft_isdigit(n[i]))
	{
		val = 10 * val + n[i] - '0';
		i++;
	}
	return ((int)(sign * val));
}
