/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_itoa.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 00:33:57 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 00:33:58 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_stdlib.h"
#include "ft_string.h"

char	*ft_itoa(int n)
{
	char	*str;
	long	abs;
	size_t	offset;

	str = (char *)ft_calloc(12, sizeof(char));
	if (str != NULL)
	{
		if (n < 0)
			abs = -1 * (long)n;
		else
			abs = (long)n;
		offset = 0;
		while (abs > 0 || offset == 0)
		{
			str[offset] = (char)(abs % 10) + '0';
			abs /= 10;
			offset++;
		}
		if (n < 0)
			str[offset] = '-';
		ft_strrev(str);
	}
	return (str);
}
