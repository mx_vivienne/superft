/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_fputln.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:06:39 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:06:43 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_string.h"

void	ft_fputln(char *s, int fd)
{
	if (s == NULL)
		return ;
	write(fd, s, ft_strlen(s));
	write(fd, "\n", 1);
}
