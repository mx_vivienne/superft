/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_fgetc.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 05:12:17 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 05:12:19 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int	ft_fgetc(int fd)
{
	unsigned char	c;

	if (read(fd, &c, 1) <= 0)
		return (EOF);
	return ((int)c);
}
