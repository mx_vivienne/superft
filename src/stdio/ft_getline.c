/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_getline.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 18:52:32 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 18:52:33 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_stdio.h"

ssize_t	ft_getline(char **lineptr, size_t *n, int fd)
{
	return (ft_getdelim(lineptr, n, '\n', fd));
}
