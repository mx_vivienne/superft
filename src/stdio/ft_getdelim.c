/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_getdelim.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 18:52:30 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 18:52:33 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "ft_stdlib.h"

static ssize_t	ft_getdelim_readfail(ssize_t ret, ssize_t offset)
{
	if (ret < 0 || offset == 0)
		return (-1);
	return (offset);
}

static ssize_t	ft_getdelim_loop(char **lineptr, size_t *n, int delim, int fd)
{
	ssize_t	offset;
	ssize_t	ret;
	char	*temp;

	offset = 0;
	while (1)
	{
		if (*n == (size_t)offset)
		{
			temp = ft_resize(*lineptr, *n, 2 * *n + 1);
			if (!temp)
				return (-1);
			*lineptr = temp;
			*n = 2 * *n + 1;
		}
		ret = read(fd, *lineptr + offset, 1);
		if (ret < 1)
			return (ft_getdelim_readfail(ret, offset));
		if ((*lineptr)[offset] == delim)
			return (offset + 1);
		offset++;
	}
}

ssize_t	ft_getdelim(char **lineptr, size_t *n, int delim, int fd)
{
	if (!lineptr || !n)
		return (-1);
	if (!*lineptr)
	{
		*lineptr = malloc(32 * sizeof(char));
		if (!*lineptr)
			return (-1);
		*n = 32;
	}
	return (ft_getdelim_loop(lineptr, n, delim, fd));
}
