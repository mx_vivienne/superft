/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_fputi.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:06:40 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:06:41 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

static void	ft_fputu(unsigned int n, int fd)
{
	char	digit;

	if (n > 9)
		ft_fputu(n / 10, fd);
	digit = '0' + (char)(n % 10);
	write(fd, &digit, 1);
}

void	ft_fputi(int n, int fd)
{
	if (n < 0)
	{
		write(fd, "-", 1);
		ft_fputu((unsigned int)(-1 * n), fd);
	}
	else
		ft_fputu((unsigned int)n, fd);
}
