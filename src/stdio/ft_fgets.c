/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_fgets.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 05:24:25 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 05:24:26 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	*ft_fgets(char *s, int size, int fd)
{
	int		offset;
	char	c;
	ssize_t	ret;

	c = 0;
	offset = 0;
	while (offset + 1 < size && c != '\n')
	{
		ret = read(fd, &c, 1);
		if (ret < 1)
		{
			if (ret == 0)
			{
				if (offset == 0)
					return (NULL);
				break ;
			}
			return (NULL);
		}
		s[offset] = c;
		offset++;
	}
	if (size > 0)
		s[offset] = '\0';
	return (s);
}
