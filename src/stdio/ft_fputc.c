/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_fputc.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:06:38 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:06:44 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_fputc(char c, int fd)
{
	write(fd, &c, 1);
}
