/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_dirname.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 23:55:10 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 23:55:12 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_dirname(char *path)
{
	char	*last;

	if (!path || !*path || !ft_strncmp(path, ".", 2)
		|| !ft_strncmp(path, "..", 3))
		return (".");
	path[ft_strlen(path) - ft_strrspn(path, "/")] = '\0';
	if (!*path)
		return ("/");
	last = ft_strrchr(path, '/');
	if (!last)
		return (path);
	return (path + 1);
}
