/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_basename.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 23:55:09 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 23:55:12 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

char	*ft_basename(char *path)
{
	char	*last;

	if (!path || !*path || !ft_strncmp(path, ".", 2))
		return (".");
	path[ft_strlen(path) - ft_strrspn(path, "/")] = '\0';
	if (!*path)
		return ("/");
	last = ft_strrchr(path, '/');
	if (!last)
		return (".");
	last[0] = '\0';
	path[ft_strlen(path) - ft_strrspn(path, "/")] = '\0';
	if (!*path)
		return ("/");
	return (path);
}
