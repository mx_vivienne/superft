/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_stdio.h                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:13:13 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:13:15 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STDIO_H
# define FT_STDIO_H

# include <unistd.h>

int		ft_fgetc(int fd);
char	*ft_fgets(char *s, int size, int fd);
void	ft_fputc(char c, int fd);
void	ft_fputi(int n, int fd);
void	ft_fputln(char *s, int fd);
void	ft_fputs(char *s, int fd);
ssize_t	ft_getdelim(char **lineptr, size_t *n, int delim, int fd);
ssize_t	ft_getline(char **lineptr, size_t *n, int fd);

#endif
