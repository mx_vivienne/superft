/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_libgen.h                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 23:53:55 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 23:53:56 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIBGEN_H
# define FT_LIBGEN_H

char	*ft_basename(char *path);
char	*ft_dirname(char *path);

#endif
