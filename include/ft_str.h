/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_str.h                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/12 01:29:58 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/12 01:30:00 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STR_H
# define FT_STR_H

# include <stddef.h>
# include <stdbool.h>

typedef struct s_string
{
	char	*content;
	size_t	capacity;
}	t_string;

bool	ft_string_init(t_string *str, size_t capacity);
bool	ft_string_from(t_string *str, char *content);
bool	ft_string_dup(t_string *original, t_string *copy);
bool	ft_string_capacity(t_string *str, size_t capacity);
void	ft_string_free(t_string *str);
bool	ft_string_replace(t_string *str, size_t start, size_t end,
			char *replacement);
bool	ft_string_insert(t_string *str, size_t index, char *src);
bool	ft_string_remove(t_string *str, size_t start, size_t end);

#endif
