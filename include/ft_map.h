/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_map.h                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/07 20:16:13 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/05/07 20:16:15 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MAP_H
# define FT_MAP_H

# include <stdbool.h>
# include "ft_list.h"

typedef struct s_pair
{
	char	*key;
	char	*value;
}	t_pair;

typedef t_list	t_map;

t_pair	*ft_pairnew(char *key, char *value);
bool	ft_pairset(t_pair **pair, char *new_value);
void	ft_pairdestroy(t_pair **pair);
int		ft_pairprint(t_pair *pair, char *format);

bool	ft_mapinsert(t_map **map, char *key, char *value);
bool	ft_mapupdate(t_map **map, char *key, char *value);
char	*ft_mapsearch(t_map *map, char *key);
t_pair	*ft_mapsearch_pair(t_map *map, char *key);
void	ft_mapremove(t_map **map, char *key);
void	ft_mapclear(t_map **map);
int		ft_mapprint(t_map *map, char *format, char *delim);
#endif
