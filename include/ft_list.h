/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_list.h                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: vwaterme <vwaterme@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/24 01:32:44 by vwaterme      #+#    #+#                 */
/*   Updated: 2021/01/24 01:32:45 by vwaterme      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIST_H
# define FT_LIST_H

typedef struct s_list
{
	void			*content;
	struct s_list	*next;
}	t_list;

void				ft_lstclear(t_list **lst, void (*del)(void*));
void				ft_lstadd_back(t_list **lst, t_list *elem);
void				ft_lstadd_front(t_list **lst, t_list *elem);
void				ft_lstdelone(t_list *lst, void (*del)(void*));
void				ft_lstiter(t_list *lst, void (*f)(void*));
t_list				*ft_lstlast(t_list *lst);
t_list				*ft_lstmap(t_list *lst, void *(*f)(void *),
						void (*del)(void *));
t_list				*ft_lstnew(void *content);
void				ft_lstremove(t_list **lst, void *item,
						int (*cmp)(void*, void*), void (*del)(void*));
t_list				*ft_lstsearch(t_list **lst, void *item,
						int (*cmp)(void*, void*));
int					ft_lstsize(t_list *lst);

#endif
