NAME = libft.a

SDIR = src
ODIR = obj
IDIR = include

CFLAGS = -Wall -Werror -Wextra -g -I$(IDIR)

CTYPE_OBJS  = ft_isalnum.o ft_isalpha.o ft_isascii.o ft_isdigit.o\
              ft_isprint.o ft_tolower.o ft_toupper.o
LIBGEN_OBJS = ft_basename.o ft_dirname.o
LIST_OBJS   = ft_lstclear.o ft_lstadd_back.o ft_lstadd_front.o ft_lstdelone.o\
              ft_lstiter.o ft_lstlast.o ft_lstmap.o ft_lstnew.o\
              ft_lstremove.o ft_lstsearch.o ft_lstsize.o
MAP_OBJS    = del_pair.o ft_mapclear.o ft_mapinsert.o ft_mapprint.o\
              ft_mapremove.o ft_mapsearch_pair.o ft_mapsearch.o ft_mapupdate.o\
              ft_pairdestroy.o ft_pairnew.o ft_pairprint.o ft_pairset.o
STDIO_OBJS  = ft_fgetc.o ft_fgets.o ft_fputc.o ft_fputi.o ft_fputln.o\
              ft_fputs.o ft_getdelim.o ft_getline.o
STDLIB_OBJS = ft_atoi.o ft_calloc.o ft_itoa.o ft_resize.o
STR_OBJS    = ft_string_capacity.o ft_string_dup.o ft_string_free.o\
              ft_string_from.o ft_string_init.o ft_string_insert.o\
              ft_string_remove.o ft_string_replace.o
STRING_OBJS = ft_bzero.o ft_isinset.o ft_memccpy.o ft_memchr.o ft_memcmp.o\
              ft_memcpy.o ft_memmove.o ft_memset.o ft_split.o ft_strchr.o\
              ft_strchrnul.o ft_strcspn.o ft_strdup.o ft_strfield.o\
              ft_strjoin.o ft_strlcat.o ft_strlcpy.o ft_strlen.o ft_strltrim.o\
              ft_strmapi.o ft_strncmp.o ft_strndup.o ft_strnlen.o ft_strnstr.o\
              ft_strrchr.o ft_strrcspn.o ft_strreplace.o ft_strrev.o\
              ft_strrspn.o ft_strrtrim.o ft_strspn.o ft_strtrim.o ft_substr.o
ALL_OBJS = $(patsubst %,$(ODIR)/%,$(CTYPE_OBJS) $(LIBGEN_OBJS) $(LIST_OBJS)\
           $(MAP_OBJS) $(STDIO_OBJS) $(STDLIB_OBJS) $(STR_OBJS) $(STRING_OBJS))

_CTYPE_DEPS  = ft_ctype.h
CTYPE_DEPS   = $(patsubst %,$(IDIR)/%,$(_CTYPE_DEPS))

_LIBGEN_DEPS = ft_libgen.h ft_string.h
LIBGEN_DEPS  = $(patsubst %,$(IDIR)/%,$(_LIBGEN_DEPS))

_LIST_DEPS   = ft_list.h
LIST_DEPS    = $(patsubst %,$(IDIR)/%,$(_LIST_DEPS))

_MAP_DEPS    = ft_map.h ft_list.h ft_string.h
MAP_DEPS     = $(patsubst %,$(IDIR)/%,$(_MAP_DEPS))

_STDIO_DEPS  = ft_stdio.h ft_stdlib.h ft_string.h
STDIO_DEPS   = $(patsubst %,$(IDIR)/%,$(_STDIO_DEPS))

_STDLIB_DEPS = ft_ctype.h ft_stdlib.h ft_string.h
STDLIB_DEPS  = $(patsubst %,$(IDIR)/%,$(_STDLIB_DEPS))

_STR_DEPS    = ft_str.h ft_string.h
STR_DEPS     = $(patsubst %,$(IDIR)/%,$(_STR_DEPS))

_STRING_DEPS = ft_string.h ft_stdlib.h
STRING_DEPS  = $(patsubst %,$(IDIR)/%,$(_STRING_DEPS))



$(NAME): $(ALL_OBJS)
	ar rcs $@ $(ALL_OBJS)

$(ODIR)/%.o: $(SDIR)/ctype/%.c $(CTYPE_DEPS)
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) -o $@ $<

$(ODIR)/%.o: $(SDIR)/libgen/%.c $(LIBGEN_DEPS)
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) -o $@ $<

$(ODIR)/%.o: $(SDIR)/list/%.c $(LIST_DEPS)
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) -o $@ $<

$(ODIR)/%.o: $(SDIR)/map/%.c $(MAP_DEPS)
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) -o $@ $<

$(ODIR)/%.o: $(SDIR)/stdio/%.c $(STDIO_DEPS)
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) -o $@ $<

$(ODIR)/%.o: $(SDIR)/stdlib/%.c $(STDLIB_DEPS)
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) -o $@ $<

$(ODIR)/%.o: $(SDIR)/str/%.c $(STR_DEPS)
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) -o $@ $<

$(ODIR)/%.o: $(SDIR)/string/%.c $(STRING_DEPS)
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) -o $@ $<



all: $(NAME)

clean:
	rm -rf $(ODIR)

fclean: clean
	rm -f $(NAME)

re: fclean all

test: $(NAME)
	make -C tests run

.PHONY: all clean fclean re
